#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cstring>
#include <cassert>
#include <vector>
#include <postgresql/libpq-fe.h>
using namespace std;

void Countries(){

/*
4
-3035823
525955
40.083333
45.916667
400500
455500
38TNK7815237409
NK38-11
T
MTS
AM
00
AM
N
Lerrnashght’a
AREVELYANSEVANILERRNASHGHTA
Arevelyan
Sevani
Lerrnashght’a
Arevelyan
Sevani
Lerrnashght'a
1998-11-02


5
-2094209
-2907452
34.9281917866238
78.6921398609104
345541
784132
44SKD8920067511
NI44-05	
S
HLT		
IN
12
V
DALCOMPAS	
Dal 
Compas	
Dal 
Compas	
2008-08-29

*/
	vector<string> file;
	file.push_back("Armenia.txt"); 
	file.push_back("Aruba.txt"); 
	file.push_back("Australia.txt"); 
	file.push_back("Ecuador.txt");
	file.push_back("France.txt");
	file.push_back("Hongkong.txt");
	file.push_back("India.txt");
	file.push_back("Kenya.txt");
	file.push_back("Nepal.txt");
	file.push_back("Norway.txt");
	file.push_back("Poland.txt");
	file.push_back("Samoa.txt"); 

	cout<<"1. Armenia"<<endl<<"2. Aruba"<<endl<<"3. Australia"<<endl<<"4. Ecuador"<<endl
		<<"5. France"<<endl<<"6. Hongkong"<<endl<<"7. India"<<endl<<"8. Kenya"<<endl
		<<"9. Nepal"<<endl<<"10. Norway"<<endl<<"11. Poland"<<endl<<"12. Samoa"<<endl;
    int n;
	cin>>n;
	assert( n>0 && n<12 ); 

	PGconn* dbconn;
    dbconn = PQconnectdb("dbname=z1787540 host=Courses port=5432 user=z1787540 password=1993Oct07");
    if (PQstatus(dbconn) == CONNECTION_BAD) {
        cout << "Bad Connection : Connection Status - " << PQerrorMessage(dbconn) << endl;
        return;
    }


    
}

void country_state(){
 	
 	PGconn* dbconn;
    dbconn = PQconnectdb("dbname=z1787540 host=Courses port=5432 user=z1787540 password=1993Oct07");
    if (PQstatus(dbconn) == CONNECTION_BAD) {
        cout << "Bad Connection : Connection Status - " << PQerrorMessage(dbconn) << endl;
        return;
    }


    string file_name = "countries.txt";
    ifstream ifs(file_name),ifs2, ifs3;;
    //trivial case for failure
    assert(ifs.good());
    PGresult* res;
  	stringstream ss;
    
	if (ifs) {
	    ss << ifs.rdbuf();    
	    ifs.close();
	}
	/*	
	0 AM	
	1 Armenia	
	2 country
	*/
	vector<vector<string>> master;
	while(ss.good()){
		vector<string> vect;
		string substr;
		getline( ss, substr );
		stringstream temp_stream(substr);
		string code,name,type;
		temp_stream>>code>>name>>type;
		vect.push_back(code);vect.push_back(name);vect.push_back(type);
		master.push_back(vect);
	}
 	string query;
 	PGresult* query_result;
 	int i;
	for ( i = 0; i < master.size(); ++i)
	{
		string location_id = master[i][0],
				name = master[i][1],
				latitude ,
				region,
				longitude ,
				location_type = master[i][2],
				data_id = location_id+name+ location_type;
		cout<<"Entering into Location Table "<<endl;
		query = "insert into z1787540.Location values('" + location_id + "','" + name + "','" + latitude + "','" + longitude + "','" + data_id + "')";
        query_result = PQexec(dbconn, query.c_str());
        if (PQresultStatus(query_result) != PGRES_COMMAND_OK) {
            cout << "Error in Location Entry :  " << PQerrorMessage(dbconn)<<endl;
            return;
        }
		
		query = "select name from z1787540.Location where data_id='" + location_id + "'";
        query_result = PQexec(dbconn, query.c_str());
        if (PQntuples(query_result) > 0) region = PQgetvalue(query_result, 0, 0);
        else region = name;
        cout<<"Entering into Region Table "<<endl;
        query = "insert into z1787540.Region values('" + location_id + "','" + location_type + "','" + region + "')";
        query_result = PQexec(dbconn, query.c_str());
        if (PQresultStatus(query_result) != PGRES_COMMAND_OK) {
            cout << "Error inserting into Region : " << PQerrorMessage(dbconn)<<endl;
        	return;
        }
        cout<<"Entering into City Table "<<endl;
        query = "insert into z1787540.City values('" + location_id + "','" + location_id + "')";
        query_result = PQexec(dbconn, query.c_str());
        if (PQresultStatus(query_result) != PGRES_COMMAND_OK) {
            cout << "Error inserting into City : " << PQerrorMessage(dbconn)<<endl;
            return;
        }		
	}

}
void US(){

	vector<string> file;
	file.push_back("AR.txt");file.push_back("ID.txt");file.push_back("IL.txt");
	file.push_back("IN.txt");file.push_back("NV.txt");file.push_back("SC.txt");

	cout << "Available data: "<<endl
		<<"1. Arkansas"<<endl<<"2. Idaho"<<endl<<"3. Illinois"
		<<endl<<"4. Indiana"<<endl<<"5. Nevada"<<endl
		<<"6. South Carolina"<<endl<<"Enter the State : ";
	int n;
	cin>>n;
	assert( n>0 && n<7 );    

	PGconn* dbconn;
    dbconn = PQconnectdb("dbname=z1787540 host=Courses port=5432 user=z1787540 password=1993Oct07");
    if (PQstatus(dbconn) == CONNECTION_BAD) {
        cout << "Bad Connection : Connection Status - " << PQerrorMessage(dbconn) << endl;
        return;
    }

    //reading the selected file
    string file_name = file[n - 1];
    ifstream ifs(file_name),ifs2, ifs3;;
    //trivial case for failure
    assert(ifs.good());
	/*

	0 1 9 10 3+4+5+6
	0 45773|
	1 Aberdeen|
	2 Populated Place|
	3 AR|
	4 05|
	5 Monroe|
	6 095|
	7 343612N|
	8 0912030W|
	9 34.6034302|
	10 -91.3417918|||||
	11 61|
	12 Aberdeen

	*/
    stringstream ss;
    PGresult* query_result;
	if (ifs) {
	    ss << ifs.rdbuf();    
	    ifs.close();
	}
	string input ;	
	vector<vector<string>> master;
	while( ss.good() )
	{
		vector<string> vect;
		string substr;
		while(getline( ss, substr, '|' )){
			
			vect.push_back( substr );
		}
		master.push_back(vect);

	}
	cout << "Pushing data into Location" << endl;
	int i;
	for ( i = 0; i < master.size(); ++i)
	{
		string location_id = master[i][0],
				name = master[i][1],
				latitude = master[i][9],
				longitude = master[i][10],
				region  = master[i][5],
				state = master[i][3],
				data_id = master[i][3]+master[i][4]+master[i][5]+master[i][6],
				insertion_querystr ,location_type,region_name; 
		
		cout<<"Entering into Location Table "<<endl;
		PGresult* query_result,*query, *insertion_query;
		insertion_querystr = "insert into z1787540.Location values('" + location_id + "','" + name + "','" + latitude + "','" + longitude + "','" + data_id + "')";
		query_result = PQexec(dbconn, insertion_querystr.c_str());
		if (PQresultStatus(query_result) != PGRES_COMMAND_OK){
			cout<<"Error Status Location Table : "<<  PQerrorMessage(dbconn)<<endl;
			return;
		}
		//Region Addition 
		location_type = "select name from z1787540.location where location_id='" + location_id + "'";
        insertion_query = PQexec(dbconn, location_type.c_str());region_name = PQgetvalue(insertion_query, 0, 0);
        location_type = "select name from z1787540.location where data_id like '%" + region_name + "%'";
        insertion_query = PQexec(dbconn, location_type.c_str());

        cout<<"Entering into Region Table "<<endl;
        if (PQntuples(insertion_query) <= 0) {
            insertion_querystr = "insert into z1787540.region values('" + location_id + "','city','" + region + "');";
            insertion_query = PQexec(dbconn, insertion_querystr.c_str());
            if (PQresultStatus(insertion_query) != PGRES_COMMAND_OK) {
                cout<< "Region table: " << PQerrorMessage(dbconn)<<endl;
                return;
            }            
        }
        else {
        	insertion_querystr = "insert into z1787540.region values('" + location_id + "','county','" + state.substr(0, 2) + "');";
            insertion_query = PQexec(dbconn, insertion_querystr.c_str());
            if (PQresultStatus(insertion_query) != PGRES_COMMAND_OK) {
                cout << "Region table: " << PQerrorMessage(dbconn)<<endl;
                return;
            }
        }
        // City operation 

        string locationid_display = "select location_id from z1787540.location where name='" + region + "'";
        query = PQexec(dbconn, locationid_display.c_str());
        string r_location_id = "";
        if (PQntuples(query) > 0) {
            r_location_id = PQgetvalue(query, 0, 0);
		
		cout<<"Entering into city Table "<<endl;
            insertion_querystr = "insert into z1787540.city values('" + location_id + "','" + r_location_id + "')";
            query = PQexec(dbconn, insertion_querystr.c_str());
            if (PQresultStatus(query) != PGRES_COMMAND_OK) {
                cout << endl
                     << "City table: " << PQerrorMessage(dbconn);
             }
        }

	}
	cout << i << " Rows Effected" << endl;
        

}

int main()
{
	
    cout << "Choose type of file :" << endl
         << "1. Unites States" << endl
         << "2. Countries" << endl
         << "3. Countries and State codes" << endl;
    int type = 0;
    cin >> type;
    switch (type) {
    case 1:
        US();
    case 2:
        Countries();
    case 3:
        country_state();
    }
    cout << "end of prog" << endl;
    return 0;
}