#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <postgresql/libpq-fe.h>
#include <cmath>
#include <stdlib.h>

using namespace std;

double distance(double lt1, double ln1, double lt2, double ln2)
{
    const double RADIUS = 2958.748;
    double b = (90 - lt1) * M_PI / 180.0;
    double c = (90 - lt2) * M_PI / 180.0;

    double a = fabs(ln1 - ln2);
    if (a > 180.0)
        a = 360 - a;

    a *= M_PI / 180.0;
    double d = acos(cos(b) * cos(c) + sin(b) * sin(c) * cos(a)) * RADIUS;
    return d;
}

int main()
{
    PGconn* dbconn;
    dbconn = PQconnectdb("dbname=z1787540 host=Courses port=5432 user=z1787540 password=1993Oct07");
    if (PQstatus(dbconn) == CONNECTION_BAD) {
        cout << "Bad Connection : Connection Status - " << PQerrorMessage(dbconn) << endl;
        return -1;
    }
    double main_distance;
    string input_city1,input_city2,c1_latitude,c2_latitude,c1_longitude,c2_longitude;
    cin>>input_city1;
    for (int i = 0; i < input_city1.size(); ++i)
    {
        input_city1[i] = tolower(input_city1[i]);
    }
    input_city1[0] = toupper(input_city1[0]);
    cin>>input_city2;
    for (int i = 0; i < input_city2.size(); ++i)
    {
        input_city2[i] = tolower(input_city2[i]);
    }
    input_city2[0] = toupper(input_city2[0]);

    PGresult* result_city1,* result_city2;

    result_city1 = PQexec(dbconn, ("select * from z1787540.location where name='" + input_city1 + "'").c_str());
    result_city2 = PQexec(dbconn, ("select * from z1787540.location where name='" + input_city2 + "'").c_str());
    bool flag1 = false,flag2 = false;

    if (PQntuples(result_city1) > 0) {
        c1_latitude = PQgetvalue(result_city1, 0, 2);
        c1_longitude= PQgetvalue(result_city1, 0, 3);
        flag1 = true;
    }
    if (PQntuples(result_city2) > 0) {
        c2_latitude = PQgetvalue(result_city2, 0, 2);
        c2_longitude = PQgetvalue(result_city2, 0, 3);
        flag2 = true;
    }
    if(flag1 || flag2){
        cout<<"Error in cities "<<endl;
        return -1;
    }
    stringstream ss;
    ss<<c1_latitude;
    double lat_c1;
    ss>>lat_c1;
    ss<<c2_latitude;
    double lat_c2 ;
    ss>>lat_c2;
    ss<<c1_longitude;
    double lon_c1;
    ss>>lon_c1;
    ss<<c2_longitude;
    double lon_c2;
    ss>>lon_c2;
    main_distance = distance(lat_c1, lon_c1, lat_c2, lon_c2);
    cout<<"distace between "<<input_city2<<" and "<<input_city1<<" is "<<main_distance<<endl;
    return 0;

}